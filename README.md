# Project Title

Sudoku solver in Julia for study Julia.

## Getting Started

Simple git clone and give it in Julia.

### Prerequisites

Julia 0.7 alpha or 1.0 install is needed.

### Installing

Just git clone only.


## Running the tests

No test at all. Simple test examples are in source.
See the source.

### Break down into end to end tests

Test examples are simple, standard and hard picked up from
web.

### And coding style tests

Not any yet

## Deployment

No description for only one source

## Built With

Just plain Julia only.

## Contributing

I'm not sure to need description here.

## Versioning

We use github.

## Authors

Nobuyuki Hikichi as bibourokushi(備忘録子), bibourokushi at gmail dot com.

## License

GPL3

## Acknowledgments

Sudoku description and problem provide web pages.

## Task list
- [ ] Check for not able to solve it especially.

## Appendix

Less cleaner Julia version is made.

Based on python version, following;
> https://github.com/bibourokushi/sudoku-py.git
